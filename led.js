var Cylon = require('cylon');

Cylon.robot({
    connections: {
        galileo: { adaptor: 'intel-iot' , port: '/dev/ttyUSB0' }
    },

    devices: {
        led: { driver: 'led', pin:13 }
    },

    work: function(my) {
        every((1).second(), function() {
            my.led.toggle();
        });
    }
}).start();
