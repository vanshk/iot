#About this Repository#
This repository holds my experiments with anything that is even remotely related to IoT ranging from python script for using Intel Galileo as a beacon to setting up a database for consequent analytics.

#Bluegiga#
Python scripts for using BLED 112 (dongle) as a scanner and as an advertiser. These scripts have been tested with the Intel Galileo.
