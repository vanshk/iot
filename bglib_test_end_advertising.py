""" Bluegiga BGAPI?BGLib implementation
=======================================
Using Python interface library, bglib.
=======================================
"""

import bglib, serial, time, datetime

# handler for API parser timeout
def my_timeout(sender, args):
    print "BGAPI parser times out. Make sure the BLE device is in a known/idle state."

# callback for disconnection
def on_disconnection(connection, reason):
    ble.send_command(ser, ble.ble_cmd_gap_set_mode(2, 2))
    ble.check_activity(ser, 1)


def main():
    port_name = "/dev/ttyACM0"
    baud_rate = 115200
    packet_mode = False

    # create BGLib object
    ble = bglib.BGLib()
    ble.packet_mode = packet_mode

    # add handler for the BGAPI timeout condition
    ble.on_timeout += my_timeout

    # add handler for the ble_evt_connection_disconnected
    ble.ble_evt_connection_disconnected += on_disconnection

    # creat serial port object and flush buffers
    ser = serial.Serial(port=port_name, baudrate=baud_rate, timeout=1)
    ser.flushInput()
    ser.flushOutput()

    print "Sending command to end advertising"
    # stop advertising if advertising already
    ble.send_command(ser, ble.ble_cmd_gap_set_mode(0, 0))
    ble.check_activity(ser, 1)
    print "Command to end advertising sent"

if __name__ == '__main__':
    main()

